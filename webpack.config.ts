import path from "path";
import { Configuration } from "webpack";
import Dotenv from "dotenv-webpack";
import HtmlWebpackPlugin from "html-webpack-plugin";
import ForkTsCheckerWebpackPlugin from "fork-ts-checker-webpack-plugin";

const webpackConfig = (): Configuration => {

    return {
        entry: "./src/index.tsx",
        resolve: {
            extensions: [".ts", ".tsx", ".js"],
            alias: {
                components: path.resolve(__dirname, "./src/components/")
            }
        },
        output: {
            path: path.join(__dirname, "/dist"),
            filename: "build.js",
            publicPath: '/'
        },
        module: {
            rules: [
                {
                    test: /\.tsx?$/,
                    loader: "ts-loader",
                    options: {
                        transpileOnly: true
                    },
                    exclude: /dist/
                },
                {
                    test: /\.(png|jpe?g|gif|jp2|webp)$/,
                    loader: "file-loader",
                    options: {
                        limit: 10000,
                        name: 'static/media/[name].[hash:8].[ext]'
                    }
                }
            ]
        },
        devServer: {
            historyApiFallback: true,
        },
        plugins: [
            new HtmlWebpackPlugin({
                template: "./public/index.html"
            }),
            new Dotenv({
                systemvars: true,
            }),
            new ForkTsCheckerWebpackPlugin({
                eslint: {
                    files: './src/**/*.{ts,tsx,js,jsx}'
                }
            })
        ]
    }
};

export default webpackConfig;