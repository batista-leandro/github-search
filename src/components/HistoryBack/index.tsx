import React, { ReactElement } from "react";
import { ButtonBase } from "@material-ui/core";
import { useHistory } from "react-router-dom";
import ArrowBack from "@material-ui/icons/ArrowBack";

interface Props {}

export default function HistoryBack({}: Props): ReactElement {
    const history = useHistory();

    return (
        <ButtonBase>
            <ArrowBack onClick={() => history.goBack()} />
        </ButtonBase>
    );
}
