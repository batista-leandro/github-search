import React, { ReactElement, useState } from "react";
import { useQuery } from "@apollo/client";
import SEARCH_USER from "../../../graphql/queries/SEARCH_USER";
import UserItem from "../UserItem/index";
import FetchMore from "../../FetchMore";
import { Grid, Box, Container } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Empty from "../../Empty";
import Loader from "../../Loader/index";
import HistoryBack from "../../HistoryBack";
import merge from "deepmerge";

interface Props {
    searchTerm: string;
}

export default function UserList({ searchTerm }: Props): ReactElement {
    const { data, loading, fetchMore } = useQuery(SEARCH_USER, {
        variables: {
            search: `${searchTerm}`
        },
        skip: searchTerm == "",
        errorPolicy: "ignore"
    });

    if (loading) return <Loader />;

    return (
        <Box bgcolor="grey.100">
            <Container maxWidth="lg">
                <Box paddingY={4} display="flex" justifyContent="space-between">
                    <HistoryBack />
                    <Box ml={2} mr="auto">
                        <Typography>
                            {data?.search.userCount
                                ? `${data?.search.userCount} Results Found`
                                : "Nothing Found"}
                        </Typography>
                    </Box>
                </Box>

                <Grid container spacing={1}>
                    {data?.search.edges.map(({ node }) => {
                        if (node.id) {
                            return (
                                <Grid item xs={6} md={4} key={node.id}>
                                    <UserItem {...node} />
                                </Grid>
                            );
                        }
                    })}
                </Grid>

                {(!data || data?.search.edges.length == 0) && (
                    <Empty text="Search for a Github User" />
                )}

                {searchTerm && (
                    <FetchMore
                        loading={loading}
                        hasNextPage={data?.search.pageInfo.hasNextPage}
                        variables={{
                            cursor: data?.search.pageInfo.endCursor
                        }}
                        updateQuery={getUpdateQuery()}
                        fetchMore={fetchMore}
                    />
                )}
            </Container>
        </Box>
    );
}

const getUpdateQuery = () => (prev, { fetchMoreResult }) => {
    if (!fetchMoreResult) {
        return prev;
    }

    return merge(prev, fetchMoreResult);
};
