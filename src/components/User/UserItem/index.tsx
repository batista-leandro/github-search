import React, { ReactElement } from "react";
import { User } from "../../../types/User";
import { Link } from "react-router-dom";
import Paper from "@material-ui/core/Paper";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import ButtonBase from "@material-ui/core/ButtonBase";
import { Button, Box } from "@material-ui/core";
import GitHubIcon from "@material-ui/icons/GitHub";
import MailIcon from "@material-ui/icons/Mail";
import Avatar from "@material-ui/core/Avatar";
import { useHistory } from "react-router-dom";

export default function UserItem({ login, email, url, avatarUrl }: User): ReactElement {
    const classes = useStyles();
    const history = useHistory();

    return (
        <Paper className={classes.paper}>
            <Box
                display="flex"
                flexDirection="column"
                alignItems="center"
                justifyContent="center"
                py={2}
                minHeight={256}
            >
                <Box display="flex" flexDirection="column" alignItems="center">
                    <a
                        href={url}
                        target="_blank"
                        rel="noopener noreferrer"
                        className={classes.link}
                    >
                        <Box display="flex" justifyContent="center">
                            <GitHubIcon className={classes.icon} />
                            <Typography style={{ textTransform: "uppercase" }}>{login}</Typography>
                        </Box>
                    </a>
                    <Link to={`/repositories/${login}`}>
                        <ButtonBase className={classes.image}>
                            <Avatar className={classes.avatar} alt="User Avatar" src={avatarUrl} />
                        </ButtonBase>
                    </Link>

                    {email && (
                        <Box color="grey.600" display="flex" alignItems="center" my={2}>
                            <Box mr={1}>
                                <MailIcon />
                            </Box>
                            <Typography>{email}</Typography>
                        </Box>
                    )}
                </Box>

                <Box mt="auto">
                    <Button
                        variant="outlined"
                        color="primary"
                        onClick={() =>
                            history.push(`/repositories/${login}`, history.location.state)
                        }
                    >
                        View Repositories
                    </Button>
                </Box>
            </Box>
        </Paper>
    );
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
        paper: {
            padding: theme.spacing(2)
        },
        image: {
            width: 128,
            height: 128
        },
        link: {
            textDecoration: "none"
        },
        avatar: {
            width: theme.spacing(12),
            height: theme.spacing(12)
        },
        icon: {
            marginRight: theme.spacing(1)
        }
    })
);
