import React, { ReactElement } from "react";
import { CircularProgress, Box } from "@material-ui/core";

interface Props {}

export default function Loader({}: Props): ReactElement {
    return (
        <Box display="flex" alignItems="center" justifyContent="center" m={4}>
            <CircularProgress />
        </Box>
    );
}
