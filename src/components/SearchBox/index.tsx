import React, { ReactElement, useState } from "react";
import { useParams, useHistory } from "react-router-dom";
import { Button, makeStyles, Theme, createStyles } from "@material-ui/core";
import InputBase from "@material-ui/core/InputBase";
import SearchIcon from "@material-ui/icons/Search";

interface Props {}

export default function SearchBox({}: Props): ReactElement {
    const [textInputvalue, setTextInputValue] = useState("");

    const { search } = useParams();

    const classes = useStyles();
    const history = useHistory();

    const navigateToSearchResult = () => history.push(`/search/${textInputvalue}`);

    const handleKey = (e) => e.keyCode === 13 && navigateToSearchResult();
    const handleChange = (e) => setTextInputValue(e.target.value);
    const handleSearch = () => navigateToSearchResult();

    return (
        <div className={classes.search}>
            <InputBase
                defaultValue={search || ""}
                placeholder="Enter a github user name…"
                onChange={handleChange}
                classes={{
                    root: classes.inputRoot,
                    input: classes.inputInput
                }}
                inputProps={{ "aria-label": "search" }}
                onKeyDown={handleKey}
            />

            <Button variant="contained" color="primary" onClick={handleSearch}>
                <SearchIcon />
            </Button>
        </div>
    );
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        search: {
            display: "flex"
        },
        inputRoot: {
            color: "inherit"
        },
        inputInput: {
            padding: theme.spacing(1, 1, 1, 0),
            paddingLeft: theme.spacing(1),
            transition: theme.transitions.create("width"),
            minWidth: "200px",
            margin: "auto",
            border: "1px solid",
            borderColor: theme.palette.grey[300],
            borderRadius: theme.shape.borderRadius,
            [theme.breakpoints.up("sm")]: {
                width: "30ch",
                "&:focus": {
                    width: "35ch"
                }
            }
        }
    })
);
