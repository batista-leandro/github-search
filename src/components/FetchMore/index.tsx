import React, { ReactElement } from "react";
import { Button } from "@material-ui/core";
import Loader from "../Loader/index";

interface Props {
    loading;
    hasNextPage;
    variables;
    updateQuery;
    fetchMore;
}

const FetchMore = ({
    loading,
    hasNextPage,
    variables,
    updateQuery,
    fetchMore
}: Props): ReactElement => (
    <div className="FetchMore">
        {loading ? (
            <Loader />
        ) : hasNextPage ? (
            <Button
                variant="contained"
                color="primary"
                style={{ width: "600px", margin: "1rem auto", display: "block" }}
                onClick={() => fetchMore({ variables, updateQuery })}
            >
                Load more...
            </Button>
        ) : (
            <Button
                variant="contained"
                color="primary"
                disabled
                fullWidth
                style={{ margin: "2rem auto", display: "block" }}
            >
                End of results
            </Button>
        )}
    </div>
);

export default FetchMore;
