import React, { ReactElement } from "react";
import Typography from "@material-ui/core/Typography";
import { makeStyles, Theme, createStyles, Box } from "@material-ui/core";
import Mascot from "../../../assets/img/mascot.png";

interface Props {
    text: string;
}

export default function Empty({ text }: Props): ReactElement {
    const classes = useStyles();

    return (
        <div className={classes.root}>
            <Box my={4}>
                <Typography variant="h4">{text}</Typography>
            </Box>
            <img className={classes.image} src={Mascot} alt="Github Mascot" />
        </div>
    );
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1,
            display: "flex",
            justifyContent: "center",
            alignItems: "center",
            padding: "4rem",
            flexDirection: "column"
        },
        image: {
            maxWidth: 256
        }
    })
);
