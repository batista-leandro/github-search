import React, { ReactElement } from "react";
import { Repo } from "../../../types/Repo";
import parse from "html-react-parser";
import Paper from "@material-ui/core/Paper";
import { makeStyles, createStyles, Theme } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import Link from "@material-ui/icons/Link";
import moment from "moment";
import { Box } from "@material-ui/core";

export default function RepositoryItem({
    name,
    nameWithOwner,
    descriptionHTML,
    url,
    updatedAt
}: Repo): ReactElement {
    const classes = useStyles();
    return (
        <Paper className={classes.paper}>
            <Box px={4}>
                <Box>
                    <Typography className={classes.link} variant="h5">
                        {name}
                    </Typography>

                    <Box display="flex" alignItems="center" my={2}>
                        <Box mr={1}>
                            <Link />
                        </Box>
                        <Box>
                            <Typography className={classes.link} variant="h6">
                                <a href={url} target="_blank" rel="noopener noreferrer">
                                    {nameWithOwner}
                                </a>
                            </Typography>
                        </Box>
                    </Box>
                </Box>

                <Box color="grey.600">
                    <Typography variant="body1">{parse(descriptionHTML)}</Typography>
                </Box>
                <Typography variant="body2" className={classes.updateTime}>
                    Last Update: {moment(updatedAt).fromNow()}
                </Typography>
            </Box>
        </Paper>
    );
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        root: {
            flexGrow: 1
        },
        paper: {
            padding: theme.spacing(2),
            margin: "0.5rem",
            minHeight: "6rem"
        },
        link: {
            "& a": {
                textDecoration: "none"
            }
        },
        updateTime: {
            marginTop: theme.spacing(4)
        }
    })
);
