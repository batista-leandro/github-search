import React, { ReactElement, useState } from "react";
import { useQuery } from "@apollo/client";
import GET_USER_REPOSITORIES from "../../../graphql/queries/GET_USER_REPOSITORIES";
import RepositoryItem from "../RepositoryItem/index";
import FetchMore from "../../FetchMore";
import {
    Grid,
    Avatar,
    makeStyles,
    Theme,
    createStyles,
    Typography,
    Box,
    Container,
    ButtonBase
} from "@material-ui/core";
import Empty from "../../Empty";
import Loader from "../../Loader/index";
import ArrowDownwardIcon from "@material-ui/icons/ArrowDownward";
import ArrowUpwardIcon from "@material-ui/icons/ArrowUpward";
import HistoryBack from "../../HistoryBack";
import merge from "deepmerge";

interface Props {
    login;
}

export default function RepositoryList({ login }: Props): ReactElement {
    const [sortDirection, setSortDirection] = useState("ASC");

    const classes = useStyles();

    const { data, loading, fetchMore, refetch } = useQuery(GET_USER_REPOSITORIES, {
        variables: {
            login,
            direction: sortDirection
        },
        skip: !login
    });

    const handleSortClick = () => {
        sortDirection === "ASC" ? setSortDirection("DESC") : setSortDirection("ASC");
        refetch();
    };

    if (loading) return <Loader />;

    return (
        <>
            <Box bgcolor="primary.main" color="primary.contrastText" py={4}>
                <Container maxWidth="lg">
                    <Box display="flex">
                        <Box>
                            <Avatar
                                alt="User Avatar"
                                src={data.user.avatarUrl}
                                className={classes.avatar}
                            />
                        </Box>
                        <Box my="auto" ml={2}>
                            <Typography variant="h4">{data.user.login}</Typography>
                            <Typography variant="subtitle1">List of Repositories</Typography>
                        </Box>
                    </Box>
                </Container>
            </Box>

            <Box bgcolor="grey.100">
                <Container maxWidth="lg">
                    <Box paddingY={4} display="flex" justifyContent="space-between">
                        <HistoryBack />

                        <Box ml={2} mr="auto">
                            <Typography>
                                {data.user.repositories.totalCount
                                    ? `${data.user.repositories.totalCount} Results Found`
                                    : "Nothing Found"}
                            </Typography>
                        </Box>
                        <Box display="flex">
                            <Typography>Sort by: Name</Typography>
                            <Box ml={2}>
                                <ButtonBase onClick={handleSortClick}>
                                    {sortDirection === "ASC" ? (
                                        <ArrowDownwardIcon />
                                    ) : (
                                        <ArrowUpwardIcon />
                                    )}
                                </ButtonBase>
                            </Box>
                        </Box>
                    </Box>

                    <Grid container>
                        {data?.user.repositories.nodes.map((node) => (
                            <Grid item xs={12} key={node.id}>
                                <RepositoryItem {...node} />
                            </Grid>
                        ))}
                    </Grid>

                    {data?.user.repositories.nodes.length == 0 && (
                        <>
                            <Empty text="No Users Found" />
                        </>
                    )}

                    <FetchMore
                        loading={loading}
                        hasNextPage={data?.user.repositories.pageInfo.hasNextPage}
                        variables={{
                            cursor: data?.user.repositories.pageInfo.endCursor
                        }}
                        updateQuery={getUpdateQuery()}
                        fetchMore={fetchMore}
                    />
                </Container>
            </Box>
        </>
    );
}

const useStyles = makeStyles((theme: Theme) =>
    createStyles({
        avatar: {
            width: 128,
            height: 128
        }
    })
);

const getUpdateQuery = () => (prev, { fetchMoreResult }) => {
    if (!fetchMoreResult) {
        return prev;
    }

    return merge(prev, fetchMoreResult);
};
