import React, { ReactElement } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import Home from "./pages/Home";
import Users from "./pages/Users";
import Repositories from "./pages/Repositories";

const App = (): ReactElement => {
    return (
        <Router>
            <Switch>
                <Route exact path="/">
                    <Home />
                </Route>
                <Route path={["/search/:search", "/search"]}>
                    <Users />
                </Route>
                <Route path="/repositories/:login">
                    <Repositories />
                </Route>
            </Switch>
        </Router>
    );
};

export default App;
