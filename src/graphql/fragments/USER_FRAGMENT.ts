import gql from "graphql-tag";

const USER_FRAGMENT = gql`
    fragment user on User {
        id
        login
        email
        url
        avatarUrl(size: 100)
    }
`;

export default USER_FRAGMENT;
