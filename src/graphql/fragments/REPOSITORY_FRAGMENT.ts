import gql from "graphql-tag";

const REPOSITORY_FRAGMENT = gql`
    fragment repository on Repository {
        id
        name
        nameWithOwner
        url
        descriptionHTML
        updatedAt
    }
`;

export default REPOSITORY_FRAGMENT;
