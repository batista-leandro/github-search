import gql from "graphql-tag";
import REPOSITORY_FRAGMENT from "../fragments/REPOSITORY_FRAGMENT";
import USER_FRAGMENT from "../fragments/USER_FRAGMENT";

const GET_USER_REPOSITORIES = gql`
    query Users($login: String!, $direction: OrderDirection!, $cursor: String) {
        user(login: $login) {
            ...user
            repositories(
                orderBy: { field: NAME, direction: $direction }
                first: 12
                after: $cursor
            ) {
                totalCount
                nodes {
                    ...repository
                }
                pageInfo {
                    endCursor
                    hasNextPage
                }
            }
        }
    }

    ${REPOSITORY_FRAGMENT}
    ${USER_FRAGMENT}
`;

export default GET_USER_REPOSITORIES;
