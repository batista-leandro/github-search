import gql from "graphql-tag";
import USER_FRAGMENT from "../fragments/USER_FRAGMENT";

const SEARCH_USER = gql`
    query Search($search: String!, $cursor: String) {
        search(query: $search, type: USER, first: 12, after: $cursor) {
            edges {
                node {
                    ...user
                }
            }
            pageInfo {
                hasNextPage
                endCursor
            }
            userCount
        }
    }

    ${USER_FRAGMENT}
`;

export default SEARCH_USER;
