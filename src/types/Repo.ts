export interface Repo {
    name: string;
    nameWithOwner: string;
    descriptionHTML: string;
    url: string;
    updatedAt: Date;
}
