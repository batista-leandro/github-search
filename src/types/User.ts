export type User = {
    id: string;
    login: string;
    email: string;
    url: string;
    avatarUrl: string;
};
