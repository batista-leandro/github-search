import React, { ReactElement } from "react";
import { useParams } from "react-router-dom";
import RepositoryList from "components/Repository/RepositoryList";

interface Props {}

export default function Repositories({}: Props): ReactElement {
    const { login } = useParams();
    return (
        <div>
            <RepositoryList login={login} />
        </div>
    );
}
