import React, { ReactElement, useState, useEffect } from "react";
import SearchBox from "components/SearchBox";
import { Container, Typography, Box } from "@material-ui/core";
import GithubImg from "../../assets/img/github.png";

interface Props {}

export default function Home({}: Props): ReactElement {
    return (
        <>
            <Container maxWidth="lg">
                <Box
                    display="flex"
                    height="95vh"
                    flexDirection="column"
                    alignItems="center"
                    justifyContent="center"
                >
                    <Box display="flex" flexDirection="column" alignItems="center">
                        <img src={GithubImg} />
                        <Box m={8}>
                            <Typography variant="h4">Search for a Github User</Typography>
                        </Box>
                        <SearchBox />
                    </Box>
                </Box>
            </Container>
        </>
    );
}
