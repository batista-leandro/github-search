import React, { ReactElement, useState, useEffect } from "react";
import UserList from "components/User/UserList";
import SearchBox from "components/SearchBox";
import { useParams, useHistory } from "react-router-dom";
import { Container, Typography, Box, ButtonBase } from "@material-ui/core";
import GithubLogo from "../../assets/img/github-logo.png";

interface Props {}

export default function Search({}: Props): ReactElement {
    const [searchTerm, setSearchTerm] = useState("");
    const { search } = useParams();
    const history = useHistory();

    useEffect(() => {
        search === undefined ? setSearchTerm("") : setSearchTerm(search);
    }, [search]);

    return (
        <>
            <Box paddingY={5}>
                <Container maxWidth="lg">
                    <Box display="flex" alignItems="center">
                        <ButtonBase onClick={() => history.push("/")}>
                            <Box mr={4}>
                                <img
                                    src={GithubLogo}
                                    alt="Github"
                                    style={{ width: "64px", maxHeight: "64px" }}
                                />
                            </Box>
                            <Box mr="auto">
                                <Typography variant="h4">Github User Search</Typography>
                            </Box>
                        </ButtonBase>
                        <Box ml="auto">
                            <SearchBox />
                        </Box>
                    </Box>
                </Container>
            </Box>
            <UserList searchTerm={searchTerm} />
        </>
    );
}
