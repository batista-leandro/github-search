# A React + Typescript + Graphql Simple Github User Search Client

This Project uses the the Github Graphql API to allow searching for a username and displaying public repositories listed by a specific user.

## Features

-   Typescript
-   React 16
-   React Router 5
-   Graphql with Apollo Client 3 (Pagination and Caching features included)
-   Material UI

## Usage

-   `git clone https://batista-leandro@bitbucket.org/batista-leandro/github-search.git`

-   cd github-search
-   Get a [GitHub personal access token](https://github.com/settings/tokens/new)
-   Check the `repo` and `user` permissions
-   Add your token to the .env.example file and rename it to .env
-   ```bash
     yarn && yarn start
    ```
-   Visit `http://localhost:3000`
## Live Demo

-   Live Demo available at [https://github-user-search-react.netlify.app/](https://github-user-search-react.netlify.app/)
